package ru.t1.aayakovlev.tm.service.model.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.service.model.ProjectTaskService;
import ru.t1.aayakovlev.tm.service.model.UserService;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectTaskServiceImpl implements ProjectTaskService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Override
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Optional<Project> project = getProjectRepository().findByUserIdAndId(userId, projectId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        @NotNull final Optional<Task> resultTask = getTaskRepository().findByUserIdAndId(userId, taskId);
        if (!resultTask.isPresent()) throw new TaskNotFoundException();
        @NotNull final Task task = resultTask.get();
        task.setProject(project.get());
        task.setUser(userService.findById(userId));
        getTaskRepository().save(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Optional<Project> project = getProjectRepository().findByUserIdAndId(userId, projectId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        @NotNull final Optional<Task> resultTask = getTaskRepository().findByUserIdAndId(userId, taskId);
        if (!resultTask.isPresent()) throw new TaskNotFoundException();
        @NotNull final Task task = resultTask.get();
        task.setProject(null);
        task.setUser(userService.findById(userId));
        return task;
    }

    @Override
    @Transactional
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull Optional<Project> project = getProjectRepository().findByUserIdAndId(userId, projectId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        getTaskRepository().deleteAllByUserIdAndProjectId(userId, projectId);
        getProjectRepository().deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        @NotNull final List<Project> projects = getProjectRepository().findAllByUserId(userId);
        for (@NotNull final Project project : projects) {
            getTaskRepository().deleteAllByUserIdAndProjectId(userId, project.getId());
        }
        getProjectRepository().deleteByUserId(userId);
    }

}
