package ru.t1.aayakovlev.tm.service.impl;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;

@Service
public final class ConnectionServiceImpl implements ConnectionService {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @NotNull
    private static Database DATABASE;

    @NotNull
    @Autowired
    private PropertyService databaseProperty;


    @NotNull
    @Override
    public Liquibase getLiquibase() {
        return new Liquibase(LIQUIBASE_CHANGELOG_FILENAME, ACCESSOR, DATABASE);
    }


    @SneakyThrows
    private void initLiquibaseConnection() {
        @NotNull final Connection connection = getConnection();
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @NotNull
    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                databaseProperty.getDatabaseURL(),
                databaseProperty.getDatabaseUser(),
                databaseProperty.getDatabasePassword()
        );
    }

}
