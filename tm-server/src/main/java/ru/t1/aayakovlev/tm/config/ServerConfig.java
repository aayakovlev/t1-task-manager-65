package ru.t1.aayakovlev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.sql.DataSource;
import java.util.Properties;

import static org.hibernate.cfg.AvailableSettings.*;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.t1.aayakovlev.tm")
@EnableJpaRepositories("ru.t1.aayakovlev.tm.repository")
public class ServerConfig {

    @NotNull
    @Autowired
    private DatabaseProperty databaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseURL());
        dataSource.setUsername(databaseProperty.getDatabaseUser());
        dataSource.setPassword(databaseProperty.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.aayakovlev.tm.dto.model", "ru.t1.aayakovlev.tm.model");
        @NotNull final Properties settings = new Properties();
        settings.put(DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DLL());
        settings.put(SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        settings.put(DEFAULT_SCHEMA, databaseProperty.getDatabaseSchema());

        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getUseRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getHZConfFile());
        factoryBean.setJpaProperties(settings);
        return factoryBean;
    }

}
