package ru.t1.aayakovlev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ExtendedService<E extends AbstractUserOwnedModel> extends BaseService<E> {

    @NotNull
    E save(
            @Nullable final String userId,
            @Nullable final E model
    ) throws AbstractException;

    @NotNull
    Collection<E> add(
            @Nullable final String userId,
            @Nullable final Collection<E> models
    ) throws AbstractException;

    void clear(@Nullable final String userId) throws AbstractException;

    long count(@Nullable final String userId) throws AbstractException;

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    List<E> findAll(@Nullable final String userId) throws AbstractException;

    @NotNull
    List<E> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<E> comparator
    ) throws AbstractException;

    @NotNull
    E findById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException;

    void remove(
            @Nullable final String userId,
            @Nullable final E model
    ) throws AbstractException;

    void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException;

    @NotNull
    E update(
            @Nullable final String userId,
            @Nullable final E model
    ) throws AbstractException;

}
