package ru.t1.aayakovlev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.TaskCreateRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Task create.";

    @NotNull
    public static final String NAME = "task-create";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskCreateListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        @NotNull final String name = nextLine();
        System.out.print("Enter description: ");
        @NotNull final String description = nextLine();

        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);

        taskEndpoint.createTask(request);
    }

}
