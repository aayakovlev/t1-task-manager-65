package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserRegistryRequest;
import ru.t1.aayakovlev.tm.dto.response.UserRegistryResponse;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Register new user.";

    @NotNull
    public static final String NAME = "user-registry";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER REGISTRY]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();
        System.out.print("Enter password: ");
        @NotNull final String password = nextLine();
        System.out.print("Enter email: ");
        @NotNull final String email = nextLine();

        @NotNull final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);

        @Nullable final UserRegistryResponse response = userEndpoint.registry(request);

        showUser(response.getUser());
    }

}
