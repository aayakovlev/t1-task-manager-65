package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserRemoveRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserRemoveListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Remove user by login.";

    @NotNull
    public static final String NAME = "user-remove";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userRemoveListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER REMOVE]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(getToken());
        request.setLogin(login);

        userEndpoint.remove(request);
    }

}
