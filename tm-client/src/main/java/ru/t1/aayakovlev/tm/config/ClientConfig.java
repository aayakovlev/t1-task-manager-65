package ru.t1.aayakovlev.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.aayakovlev.tm.endpoint.*;
import ru.t1.aayakovlev.tm.service.PropertyService;

@Configuration
@ComponentScan("ru.t1.aayakovlev.tm")
public class ClientConfig {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Bean
    @NotNull
    public AuthEndpoint authEndpoint() {
        @NotNull final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);
        return authEndpoint;
    }

    @Bean
    @NotNull
    public SystemEndpoint systemEndpoint() {
        @NotNull final SystemEndpoint systemEndpoint = SystemEndpoint.newInstance(propertyService);
        return systemEndpoint;
    }

    @Bean
    @NotNull
    public ProjectEndpoint projectEndpoint() {
        @NotNull final ProjectEndpoint projectEndpoint = ProjectEndpoint.newInstance(propertyService);
        return projectEndpoint;
    }

    @Bean
    @NotNull
    public TaskEndpoint taskEndpoint() {
        @NotNull final TaskEndpoint taskEndpoint = TaskEndpoint.newInstance(propertyService);
        return taskEndpoint;
    }

    @Bean
    @NotNull
    public UserEndpoint userEndpoint() {
        @NotNull final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);
        return userEndpoint;
    }

    @Bean
    @NotNull
    public DomainEndpoint domainEndpoint() {
        @NotNull final DomainEndpoint domainEndpoint = DomainEndpoint.newInstance(propertyService);
        return domainEndpoint;
    }

}
