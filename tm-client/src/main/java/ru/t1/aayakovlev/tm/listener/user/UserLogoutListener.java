package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserLogoutRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Logout user session.";

    @NotNull
    public static final String NAME = "logout";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER LOGOUT]");

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());

        authEndpoint.logout(request);
        setToken(null);
    }

}
