package ru.t1.aayakovlev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    long count() throws AbstractException;

    @Transactional
    void deleteAll();

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<Project> findAll() throws AbstractException;

    @NotNull
    Project findById(@Nullable final String id) throws AbstractException;

    @NotNull
    Project save(@Nullable final Project project) throws EntityEmptyException;

}
