package ru.t1.aayakovlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

public interface IProjectRestEndpoint {

    long count() throws AbstractException;

    void deleteById(@NotNull final String id) throws AbstractException;

    boolean existsById(@NotNull final String id) throws AbstractException;

    @NotNull
    List<ProjectDTO> findAll() throws AbstractException;

    @NotNull
    ProjectDTO findById(@NotNull final String id) throws AbstractException;

    @NotNull
    ProjectDTO save(@NotNull final ProjectDTO project) throws EntityEmptyException;

    @NotNull
    ProjectDTO update(@NotNull final ProjectDTO project) throws EntityEmptyException;

}
