package ru.t1.aayakovlev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

public interface ITaskRestEndpoint {

    long count() throws AbstractException;

    void delete();

    void deleteById(@NotNull final String id) throws AbstractException;

    boolean existsById(@NotNull final String id) throws AbstractException;

    @NotNull
    List<TaskDTO> findAll() throws AbstractException;

    @NotNull
    TaskDTO findById(@NotNull final String id) throws AbstractException;

    @NotNull
    TaskDTO save(@NotNull final TaskDTO task) throws EntityEmptyException;

    @NotNull
    TaskDTO update(@NotNull final TaskDTO task) throws EntityEmptyException;

}
