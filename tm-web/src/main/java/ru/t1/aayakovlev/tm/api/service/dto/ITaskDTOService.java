package ru.t1.aayakovlev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

import java.util.List;

public interface ITaskDTOService {


    long count() throws AbstractException;

    @Transactional
    void deleteAll();

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<TaskDTO> findAll() throws AbstractException;

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws AbstractException;

    @NotNull
    TaskDTO findById(@Nullable final String id) throws AbstractException;

    @NotNull
    TaskDTO save(@Nullable final TaskDTO task) throws EntityEmptyException;

}
