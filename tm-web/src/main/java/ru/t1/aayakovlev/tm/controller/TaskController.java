package ru.t1.aayakovlev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.repository.ProjectDTORepository;

@Controller
@RequestMapping("/tasks")
public final class TaskController {

    @Autowired
    private ITaskDTOService service;

    @Autowired
    private ProjectDTORepository projectRepository;

    @GetMapping("")
    public ModelAndView index() throws AbstractException {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", service.findAll());
        modelAndView.addObject("projectRepository", projectRepository);
        return modelAndView;
    }

    @GetMapping("/create")
    public String create() throws EntityEmptyException {
        service.save(new TaskDTO("new Task " + System.currentTimeMillis()));
        return "redirect:/tasks";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") String id) throws AbstractException {
        service.deleteById(id);
        return "redirect:/tasks";
    }

    @PostMapping("/edit/{id}")
    public String edit(
            @ModelAttribute("task") final TaskDTO task,
            final BindingResult result
    ) throws EntityEmptyException {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        service.save(task);
        return "redirect:/tasks";
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") final String id) throws AbstractException {
        final TaskDTO task = service.findById(id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectRepository.findAll());
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
